import React, { Component } from "react";

class Disclaimer extends Component {
  state = {
    disclaimer: "Fetching Disclaimer",
  };
  componentDidMount() {
    fetch("https://api.coindesk.com/v1/bpi/currentprice.json")
      .then((res) => res.json())
      .then((data) => {
        this.setState({ disclaimer: data.disclaimer });
      });
  }

  render() {
    return (
      <p
        style={{
          background: "#3f50b5",
          padding: "20px",
        }}
      >
        Disclaimer: {this.state.disclaimer}
      </p>
    );
  }
}

export default Disclaimer;
