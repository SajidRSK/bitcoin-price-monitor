import React, { Component } from "react";
import {
  Table,
  TableBody,
  TableCell,
  Paper,
  TableRow,
  TableHead,
  TableContainer,
} from "@material-ui/core";

import { ArrowDownward, ArrowUpward, DragHandle } from "@material-ui/icons";

export class TrackTable extends Component {
  state = {
    tableData: [],
  };

  updateTableData = (USD, EUR, GBP, time) => {
    const oldTable = this.state.tableData;
    const newData = { USD, EUR, GBP, time };
    const updatedTableData = [newData, ...oldTable];
    this.setState({ tableData: updatedTableData });
  };

  fetchData = () => {
    fetch("https://api.coindesk.com/v1/bpi/currentprice.json")
      .then((res) => res.json())
      .then((data) => {
        const [USD, EUR, GBP, time] = [
          data.bpi.USD,
          data.bpi.EUR,
          data.bpi.GBP,
          data.time,
        ];
        this.updateTableData(USD, EUR, GBP, time);
      });
  };

  componentDidMount() {
    this.fetchData();
    setInterval(this.fetchData, 30000);
  }

  render() {
    let table = <p>LOading</p>;
    if (this.state.tableData) {
      const tableData = this.state.tableData.slice(0, 10);
      table = (
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Time</TableCell>
                <TableCell>Change</TableCell>
                <TableCell>USD</TableCell>
                <TableCell>BGP</TableCell>
                <TableCell>EUR</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {tableData.map((row, index, array) => {
                let change = <DragHandle style={{ color: "#333" }} />;
                if (index > 1) {
                  const prevIndex = index - 1;
                  if (
                    array[index].USD.rate_float >
                    array[prevIndex].USD.rate_float
                  ) {
                    change = <ArrowDownward color="error" />;
                  } else if (
                    array[index].USD.rate_float <
                    array[prevIndex].USD.rate_float
                  ) {
                    change = <ArrowUpward style={{ color: "green" }} />;
                  }
                }

                return (
                  <TableRow key={index}>
                    <TableCell component="th" scope="row">
                      {row.time.updated}
                    </TableCell>
                    <TableCell>{change}</TableCell>
                    <TableCell>{row.USD.rate}</TableCell>
                    <TableCell>{row.GBP.rate}</TableCell>
                    <TableCell>{row.EUR.rate}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      );
    }

    return table;
  }
}

export default TrackTable;
