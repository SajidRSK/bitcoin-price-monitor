import React from "react";
import { AppBar, Typography } from "@material-ui/core";

const Navbar = () => {
  return (
    <AppBar
      style={{
        padding: "10px",
      }}
      position="static"
      color="primary"
    >
      <Typography variant="h4">Bitcoin Price Monitor</Typography>
    </AppBar>
  );
};

export default Navbar;
