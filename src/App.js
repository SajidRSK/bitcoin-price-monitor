import React from "react";

import "./App.scss";
import Navbar from "./components/Navbar";
import TrackTable from "./components/TrackTable";
import Disclaimer from "./components/Disclaimer";

function App() {
  return (
    <div className="app">
      <Navbar />
      <TrackTable />
      <Disclaimer />
    </div>
  );
}

export default App;
